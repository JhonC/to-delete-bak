# DOCUMENTATION

## GREETING API SERVICES

### POST
```
http://localhost:3000/greeting
{
	"description": "Bonjour ... "
}
```

### GET
```
http://localhost:3000/hello
http://localhost:3000/hello/:name
http://localhost:3000/greeting
http://localhost:3000/greeting/:id

```

## USER API SERVICES

### POST
```
http://localhost:3000/user/register
{
	"nickname": "user",
    "password": "pass",
    "lat": -24.188738,
    "lon": -65.303496
}

http://localhost:3000/user/login
{
	"nickname": "user"
	"password": "pass"
}

http://localhost:3000/user/logout
{
	"id": "1"
}

http://localhost:3000/user/password
{
	"id": "1"
	"password": "pass"
}

```

### GET
```
// user authentication
http://localhost:3000/user/:username
http://localhost:3000/user/:id

```
## QUERY ENGINE SERVICES

### GET
```
// person
http://localhost:3000/persons
```

### POST
```
http://localhost:3000/persons
{
	"name": "pepito",
	"age": 13,
	"bornDate": "2014-03-01"
}

http://localhost:3000/persons/query
{
 	"type": "list",
 	"model" : {
 		"name": {"beginsWith": "p"},
 		"age": {"lessThan": 20},
 		"id": {"equals": 3},
		"bornDate": {"lessThan": "2019-02-04"}
 	}
 }
```
