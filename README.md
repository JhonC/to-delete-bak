# DOCUMENTATION

## TEST
```
docker-compose run web npm run runTests
```

## DEVELOPMENT CONFIGURATION
This section will lead you to configure your local machine for development. For fast configuration we use `Docker` for its benefits of portability. It configures the `nodejs` server and `mysql` server.

Installation of `Docker` is not contempled in this document. The Docker version is we tested is `Docker 19.03.5` . After installing it we follow the next steps.

`docker-compose up`

As is the first time, this will take some time, we must wait until MySql loads and configure all its dependencies after MySql has been configurated let's create the tables.

```
ERROR: for database  Cannot start service database: driver failed programming external connectivity on endpoint nodejs-base-project_database_1 (ef0560a7f9fe2f99d56df7a7a788f31c1001188d70f7e7675612ce98528e9d1d): Error starting userland proxy: listen tcp 0.0.0.0:3306: bind: address already in use

# Change database port on docker-compose.yml
```

```
# Then run gulp
```
`docker-compose run web gulp compile`

```


```
# then create de database by running the command outside the container `web`
docker-compose run web npm run db_init

# or manually run the command below inside the container `web`
npm run db_init
```

Note, sometimes the configuration is not working for database connection. Therefore we must configure the dataset manually, you must look at the section of `Database configuration` below.

This are all steps needed to configure the development environment.

```
# to restart and update changes run:
docker-compose run web npm run db_init
docker-compose run web gulp compile
docker-compose restart web
```

### Dump the database

```
# create de database by running the command outside the container `web`
docker-compose run web npm run db_init

# or manually run the command below inside the container `web`
npm run db_init
```

```
# create de database by running the command outside the container `web`
docker-compose run web npm run db_init

# or manually run the command below inside the container `web`
npm run db_init
```


### Useful commands

Build an image and force stop the running containers. It is useful after we remove the images and containers.
```
docker build --force-rm ./
```

Kill a process in docker.
```
# stop a the Docker
docker stop $(docker ps -aq)

# or stop the container
docker stop {container_id}
```

### API configuration
In docker the command `docker-composer up` runs the nodejs server api. All docker configuration are writen in `DockerFile` and `docker-compose.yml`

Manually the next commands are used to run the nodejs server api.

`gulp compile` and `npm run db_init`

`npm start` or `node dist/index.js`

after compile .ts files, the commands above will run express.

### Database Error connection Mysql 8.0

```
## into bash command line
> docker exec -i -t 5672 /bin/sh

# start mysql console and query users
> mysql -u root -p
> select User, Host from mysql.user

## change credentials in mysql
ALTER USER 'db_user' IDENTIFIED WITH mysql_native_password BY 'db_pass'
## note that the passwords was already setted up.



```

### Database configuration

Create a MySql scheme in Docker container. Note that the username and password is on the `docker-compose.yml` file.

```
## First, copy the dbscripts/create.sql
docker cp ./dbscripts/create.sql <container_id>:/

## Second, get into sh command
docker exec -i -t 5672 /bin/sh

# and run inside docker container
mysql -u db_user -p -D db_dev < /create.sql

## or run this outside the container
docker exec -it 5672  /bin/sh -c 'mysql -u db_user -p<password> -D db_dev < /create.sql'
```

Docker saves the data as a `volume` inside de container. To save the data inside a host folder, just uncomment the line `# - ./mysql:/var/lib/mysql` in `docker-compose.yml`. In the configuration the storing folder is on './mysql'

To remove all the configuration of Mysql
	i) for host data save configuration remove the 'mysql' folder in the host.
	ii) for data saved in docker container remove the '/var/lib/mysql' in the container using the commands below.

```
docker exec -i -t {docker container id} /bin/sh
rm -R /var/lib/mysql/*
```

Side note, the dabase scripts in the database container can run in the `web` conteiner by the scripts below.

`npm run db_init` to run sql scripts in the file `dbscripts/create.sql`

`npm run db_clean` to run sql scripts in the file`dbscripts/truncate.sql`


## PRODUCTION (in progress)

Configuration files

`firebaseKeys.json` firebase admin Json file, it must be replaced by the production server

`config.ts` this file defines configuration fields for, MySql, firebase, express

docker-composer configures `database` and `web` services.

### WEB

We create a configuration to work with `database` service defined avobe, it also have defined the database credentials on dev mode (to configure the project on production change the `/src/config.ts` file and compile)
