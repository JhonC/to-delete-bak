import * as fs from 'fs'
import DBConnection from './db/DBConnection'

// define params
const CREATE: string = "--create"
const CLEAN: string = "--clean"

console.log("using arg: " + process.argv)
console.log("using arg: " + process.argv[2])
let arg = process.argv[2]


let file = 'dbscripts/create.sql'

if (arg === CREATE) {
    file = 'dbscripts/create.sql'
} else if (arg === CLEAN) {
    file = 'dbscripts/truncate.sql'
} else {
    console.error("dbInit Invalid Argument")
    process.exit()
}

fs.readFile(file, 'utf8', function (err,data) {

    if (err) {
        console.log(err)
        return 
    }

    let db: DBConnection = new DBConnection()

    console.log("Running create.sql")
    console.log(data)

    db.executeSqlFile(data, 
        (result: object)=> {
            console.error(result)
            process.exit()
        },
        (result: object)=> {
            console.log(result)
            process.exit()
        })
  
})


