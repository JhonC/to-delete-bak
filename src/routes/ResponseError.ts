export interface ResponseError {
    code: number
    error: ResponseErrorMessage
}

export interface ResponseErrorMessage {
    message: any
}