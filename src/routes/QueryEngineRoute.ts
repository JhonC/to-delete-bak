import { NextFunction, Request, Response, Router } from "express"
import { BaseRoute } from "./BaseRoute"
import { ResponseError } from "./ResponseError"
import PersonService from "../db/services/PersonService"

export default class QueryEngineRoute extends BaseRoute {

    public personService: PersonService

    /**
     * Constructor
     */
    constructor() {
        super()
        this.personService = new PersonService
    }

    /**
     * register this route
     * @param router
     */
    public static register(router: Router) {
        //log
        console.log("[IndexRoute::create] Creating index route.")

        let route:QueryEngineRoute = new QueryEngineRoute()
        // add route
        router.post("/persons", (req: Request, res: Response, next: NextFunction) => {
            route.createPerson(req, res, next)
        })
        router.get("/persons", (req: Request, res: Response, next: NextFunction) => {
            route.getAll(req, res, next)
        })
        // person query
        router.post("/persons/query", (req: Request, res: Response, next: NextFunction) => {
            route.queryPerson(req, res, next)
        })
    }

    public getAll(req: Request, res: Response, next: NextFunction) {
        this.personService.findAll(
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public createPerson(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.personService.create(req.body,
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public queryPerson(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.personService.query(req.body,
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

}
