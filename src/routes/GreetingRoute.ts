import { NextFunction, Request, Response, Router } from "express"
import { BaseRoute } from "./BaseRoute"
import { ResponseError } from "./ResponseError"
import GreetingService from "../db/services/GreetingService"

export default class GreetingRoute extends BaseRoute {

    public greetingService: GreetingService

    /**
     * Constructor
     */
    constructor() {
        super()
        this.greetingService = new GreetingService
    }

    /**
     * register this route
     * @param router
     */
    public static register(router: Router) {
        //log
        console.log("[IndexRoute::create] Creating index route.")

        let route:GreetingRoute = new GreetingRoute()
        //add home page route
        router.get("/hello", (req: Request, res: Response, next: NextFunction) => {
            route.helloWorld(req, res, next)
        })

        router.post("/greeting/", (req: Request, res: Response, next: NextFunction) => {
            route.createGreeting(req, res, next)
        })

        router.get("/greeting", (req: Request, res: Response, next: NextFunction) => {
            route.getAll(req, res, next)
        })

        router.get("/greeting/:id", (req: Request, res: Response, next: NextFunction) => {
            route.getById(req, res, next)
        })

        router.get("/hello/:name", (req: Request, res: Response, next: NextFunction) => {
            route.hello(req.params.name, req, res, next)
        })
    }

    public getById(req: Request, res: Response, next: NextFunction) {
        this.greetingService.findById(Number(req.params.id), 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(users: object)=>{ // success
                res.json(users)
                next()
            })
    }

    public getAll(req: Request, res: Response, next: NextFunction) {
        this.greetingService.findAll( 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(users: object)=>{ // success
                res.json(users)
                next()
            })
    }

    /**
     * handles /hello route
     *
     * @param req
     * @param res
     * @param next
     */
    public helloWorld(req: Request, res: Response, next: NextFunction) {
        res.json({
            message: "Hello World!"
        })
    }

    /**
     * handles /hello/:name route
     *
     * @param name param defined on the request
     * @param req
     * @param res
     * @param next
     */
    public hello(name: String, req: Request, res: Response, next: NextFunction) {
        res.json({
            message: "Hello " + name
        })
    }

    /**
     * handles /hello/:name route
     *
     * @param req
     * @param res
     * @param next
     */
    public createGreeting(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.greetingService.create(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

}
