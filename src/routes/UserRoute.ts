import { NextFunction, Request, Response, Router } from "express"
import { BaseRoute } from "./BaseRoute"
import { ResponseError } from "./ResponseError"
import UserService from "../db/services/UserService"

export default class UserRoute extends BaseRoute {

    public userService: UserService

    /**
     * Constructor
     */
    constructor() {
        super()
        this.userService = new UserService
    }

    /**
     * register this route
     * @param router 
     */
    public static register(router: Router): void {
        //log
        console.log("[IndexRoute::create] Creating index route.")

        let route: UserRoute = new UserRoute()

        //add routes
        router.get("/user", (req: Request, res: Response, next: NextFunction) => {
            route.getAll(req, res, next)
        })

        router.get("/user/:id(\\d+)", (req: Request, res: Response, next: NextFunction) => {
            route.getUserById(req, res, next)
        })

        router.get("/user/:nickname", (req: Request, res: Response, next: NextFunction) => {
            route.getUserByNickname(req, res, next)
        })

        router.post("/user/password", (req: Request, res: Response, next: NextFunction) => {
            route.updatePassword(req, res, next)
        })

        router.post("/user/location", (req: Request, res: Response, next: NextFunction) => {
            route.registerDeviceLocation(req, res, next)
        })
        
        router.post("/user/registerAnonymous", (req: Request, res: Response, next: NextFunction) => {
            route.registerAsAnonymousUser(req, res, next)
        })

        router.post("/user/register", (req: Request, res: Response, next: NextFunction) => {
            route.registerBrandNewUser(req, res, next)
        })

        router.post("/user/login", (req: Request, res: Response, next: NextFunction) => {
            route.login(req, res, next)
        })

        router.post("/user/logout", (req: Request, res: Response, next: NextFunction) => {
            route.logout(req, res, next)
        })

        // router.post("/user/logout", (req: Request, res: Response, next: NextFunction) => {
        //     route.bulding(req, res, next)
        // })
        
        router.post("/user/delete", (req: Request, res: Response, next: NextFunction) => {
            route.deleteUser(req, res, next)
        })
        
        router.post("/user/updateLastViewed", (req: Request, res: Response, next: NextFunction) => {
            route.updateLastViewed(req, res, next)
        })
    }

    public getAll(req: Request, res: Response, next: NextFunction) {
        this.userService.getAll( 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(users: object)=>{ // success
                res.json(users)
                next()
            })
    }

    public login(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.login(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public logout(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.logout(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public updatePassword(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.updatePassword(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public getUserById(req: Request, res: Response, next: NextFunction) {
        this.userService.getUserById(Number(req.params.id), 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(users: object)=>{ // success
                res.json(users)
                next()
            })
    }

    public getUserByNickname(req: Request, res: Response, next: NextFunction) {
        this.userService.getUserByNickname(req.params.nickname, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(users: object)=>{ // success
                res.json(users)
                next()
            })
    }

    public registerDeviceLocation(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.registerDeviceLocation(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

    public registerBrandNewUser(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.registerBrandNewUser(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
    
    public registerAsAnonymousUser(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.registerAnonymousUser(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
    
    public deleteUser(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.deleteUser(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
    
    public updateLastViewed(req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        this.userService.updateLastViewed(req.body, 
            (error: ResponseError)=>{ // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }

}