import { NextFunction, Request, Response } from "express"
import { ResponseError } from "./ResponseError"

export class BaseRoute {

    public configure(){}

    /**
     * handle parsed error
     * 
     * @param req 
     * @param error 
     */
    public handleError(res: Response, error: ResponseError) {
        console.error(error)
        res.status(error.code).json(error.error)
    }

    /**
     * default building message
     * 
     * @param req 
     * @param res 
     * @param next 
     */
    public bulding(req: Request, res: Response, next: NextFunction) {
        res.json({
            message: "Building....!"
        })
    }

}
