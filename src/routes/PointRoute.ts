import { NextFunction, Request, Response, Router } from "express"
import { BaseRoute } from "./BaseRoute"
import { ResponseError } from "./ResponseError"
import PointService from "../db/services/PointService"

export default class PointRoute extends BaseRoute {

    private pointService: PointService

    /**
     * Constructor
     */
    constructor() {
        super()
        this.pointService = new PointService
    }

    /**
     * register this route
     * @param router
     */
    public static register(router: Router): void {
        //log
        console.log("[IndexRoute::create] Creating index route.")

        let route: PointRoute = new PointRoute()

        // create routes
        router.get("/points", (req: Request, res: Response, next: NextFunction) => {
            route.fetchPoints(req, res, next)
        })
        router.get("/points/:lat&:lon", (req: Request, res: Response, next: NextFunction) => {
            route.fetchPointsByLatLon(req, res, next)
        })
        router.get("/points/:lat&:lon&:rad", (req: Request, res: Response, next: NextFunction) => {
            route.fetchPointsByRadius(req, res, next)
        })
        router.get("/points/:id", (req: Request, res: Response, next: NextFunction) => {
            route.fetchPointsById(req, res, next)
        })

        router.post("/points", (req: Request, res: Response, next: NextFunction) => {
            route.createPoint(req, res, next)
        })

        router.delete("/points/:id", (req: Request, res: Response, next: NextFunction) => {
            route.deletePoint(req, res, next);
        })

    }

    public fetchPoints(req: Request, res: Response, next: NextFunction) {
        this.pointService.findPoints(
            (error: ResponseError) => { // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
    public fetchPointsByRadius(req: Request, res: Response, next: NextFunction) {
        this.pointService.findPointsByRadius(
            Number(req.params.lat),
            Number(req.params.lon),
            Number(req.params.rad),
            (error: ResponseError) => { // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                console.log(result)
                res.json(result)
                next()
            })
    };
    public fetchPointsByLatLon(req: Request, res: Response, next: NextFunction) {
        this.pointService.findPointsByLatLon(Number(req.params.lat), Number(req.params.lon),
            (error: ResponseError) => { // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
    public fetchPointsById(req: Request, res: Response, next: NextFunction) {
        this.pointService.findPointsById(Number(req.params.id),
            (error: ResponseError) => { // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                console.log(result)
                res.json(result)
                next()
            })
    }


    public deletePoint(req: Request, res: Response, next: NextFunction) {
        this.pointService.deletePoint(Number(req.params.id),
            (error: ResponseError) => {
                super.handleError(res, error);
            },(result: object) => {
                res.json(result)
                next()
            })
    }

    public createPoint(req: Request, res: Response, next: NextFunction) {
        this.pointService.createPoint(req.body,
            (error: ResponseError) => { // error
                super.handleError(res, error)
                next()
            },(result: object)=>{ // success
                res.json(result)
                next()
            })
    }
}
