import ExampleTest from './tests/ExampleTest'
import QueryEngineTests from './tests/QueryEngineTests'
import QuerySQLEngineTests from './tests/QuerySQLEngineTests'

/**
 * run tests
 */
function runTest(title: string, clazz: any){
    try{
        (new clazz).run()
        console.info(title + '... OK')
    } catch (e){
        throw e;
    }

}

function main() {
    console.log("******** Running tests  ********");
    runTest('ExampleTest', ExampleTest)
    runTest('QueryEngineTests', QueryEngineTests)
    runTest('QuerySQLEngineTests', QuerySQLEngineTests)
    console.log("******** Tests finished ********");
}


main();
