import ParamsJsonElement from '../elements/ParamsJsonElement'
import MysqlQueryBuilder from './MysqlQueryBuilder'

/**
 * QueryBuilder abstract class that
 */
export default abstract class QueryBuilder {

	public abstract setTarget(e: string): void;
	public abstract setType(e: string): void;
	public abstract addParam(e: ParamsJsonElement): void;

	public abstract build() : any;

	public static newDefault(): QueryBuilder{
		 return new MysqlQueryBuilder()
	}
}
