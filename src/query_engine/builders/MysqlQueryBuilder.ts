import QueryBuilder from './QueryBuilder'
import ParamsJsonElement from '../elements/ParamsJsonElement'
import {ParamType, ParamNumber, ParamString, ParamDate } from '../schemes/paramScheme'


const SQL = {
	SELECT : ' SELECT ',
	WHERE : ' WHERE ',
	FROM : ' FROM ',
	LIKE : ' LIKE ',
	AND : ' AND ',
	OR : ' OR ',
	LESS_THAN : ' < ',
	GREATHER_THAN : ' > ',
	EQUALS : ' = '
}
/**
 * Mysql  builder
 */
export default class MysqlQueryBuilder extends QueryBuilder {

	public target: string
	public type: string
	public params: Array<ParamsJsonElement> = []

	public setTarget(e: string): void {
		this.target = e
	}

	public setType(e: string): void {
		this.type = e
	}

	public addParam(e: ParamsJsonElement): void {
		this.params.push(e)
	}

	public build(): any {
		return SQL.SELECT
			+ '*'
			+ SQL.FROM
			+ this.target
			+ SQL.WHERE
			+ this.buildWhere()
	}

	public buildWhere(): string {
		var sqlwhere: Array<string> = this.params.map((param) => {
			switch (param.getType()) {
				case ParamType.string:
					return this.buildStringParam(param)
				case ParamType.number:
					return this.buildNumberParam(param)
				case ParamType.date:
						return this.buildDateParam(param)
			}
		})
		return sqlwhere.join(SQL.AND)
	}

	public buildStringParam(param: ParamsJsonElement): string {
		var res : string = ''
		var p : any = param.getParamObject()
		if( p.hasOwnProperty(ParamString.endsWith)
			|| p.hasOwnProperty(ParamString.beginsWith)){

			if( p.hasOwnProperty(ParamString.endsWith)){
				res = res + param.key
						+ SQL.LIKE
						+ `'%` + p[ParamString.endsWith] + `'`
			}
			if( p.hasOwnProperty(ParamString.beginsWith)) {
				res = res
						+ (res != '' ? SQL.OR : '')
						+ param.key
						+ SQL.LIKE
						+ `'` + p[ParamString.beginsWith] + `%'`
			}
		} else if (p.hasOwnProperty(ParamString.equals)) {
			res = res + param.key
					+ SQL.EQUALS
					+ `'` + p[ParamString.equals] + `'`
		}

		return res
	}

	public buildNumberParam(param: ParamsJsonElement): string {
		var res : string = ''
		var p : any = param.getParamObject()
		if( p.hasOwnProperty(ParamNumber.lessThan) ){
			res = res + param.key
					+ SQL.LESS_THAN
					+ p[ParamNumber.lessThan]
			if( p.hasOwnProperty(ParamNumber.greatherThan)) {
				res = res + param.key
						+ SQL.GREATHER_THAN
						+ p[ParamNumber.greatherThan]
			}
		} else if (p.hasOwnProperty(ParamNumber.equals)) {
			res = res + param.key
					+ SQL.EQUALS
					+ p[ParamNumber.equals]
		}

		return res
	}

	public buildDateParam(param: ParamsJsonElement): string {
		var res : string = ''
		var p : any = param.getParamObject()
		if( p.hasOwnProperty(ParamDate.lessThan) ){
			res = res + param.key
					+ SQL.LESS_THAN
					+ this.parsedDate(p[ParamDate.lessThan])
			if( p.hasOwnProperty(ParamDate.greatherThan)) {
				res = res + param.key
						+ SQL.GREATHER_THAN
						+ this.parsedDate(p[ParamDate.greatherThan])
			}
		} else if (p.hasOwnProperty(ParamDate.equals)) {
			res = res + param.key
					+ SQL.EQUALS
					+ this.parsedDate(p[ParamDate.equals])
		}

		return res
	}

	private parsedDate(date: string) :string {
		return `STR_TO_DATE('${date}','%Y-%m-%d')`
	}

}
