import QueryJsonElement from '../elements/QueryJsonElement'
import ModelJsonElement from '../elements/ModelJsonElement'
import ParamsJsonElement from '../elements/ParamsJsonElement'
import Visitor from './Visitor'

/**
 * Visitor class that
 */

export default class KeyValidationVisitor extends Visitor {

	public visitQuery(el: QueryJsonElement){
		var query = el.getQueryBuilder();
		query.setTarget(el.getTarget())
		query.setType(el.getType())
	}

	public visitModel(el: ModelJsonElement){
	}

	public visitParam(el: ParamsJsonElement){
		var query = el.getQueryBuilder();
		query.addParam(el)
	}

}
