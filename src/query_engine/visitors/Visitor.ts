import Element from '../elements/Element'
import Client from '../Client'

/**
 * Visitor abstract class that
 */

export default abstract class Visitor extends Client {
	public abstract visitQuery(e: Element): any;
	public abstract visitModel(e: Element): any;
	public abstract visitParam(e: Element): any;
}
