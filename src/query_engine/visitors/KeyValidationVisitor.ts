import QueryJsonElement from '../elements/QueryJsonElement'
import ModelJsonElement from '../elements/ModelJsonElement'
import ParamsJsonElement from '../elements/ParamsJsonElement'
import Visitor from './Visitor'
import {Query, QueryType } from '../schemes/queryScheme'
import {ParamType, ParamNumber, ParamString, ParamDate } from '../schemes/paramScheme'

/**
 * Visitor class that
 */

export default class KeyValidationVisitor extends Visitor {

	public visitQuery(el: QueryJsonElement){
		var object = el.getModelObject();
		if(!object.hasOwnProperty(Query.type)) {
			el.setSchemeAttr(Query.type, QueryType.list)
		}

		if(!object.hasOwnProperty(Query.model)) {
			console.log(object)
			this.throwRequiresError(Query.model)
		}
	}

	public visitModel(el: ModelJsonElement){
		var scheme = el.getScheme();
		var object: any = el.getModelObject();
		// validate keys
		Object.keys(object).map((key: any)=> {
			if(!scheme.hasOwnProperty(key)) {
				this.throwMustNotContainError(key)
			}
		})

		// create params
		Object.keys(object).map((key: any)=> {
			el.addParam(key,scheme[key], object[key], this.getParamSchemeFromType(scheme[key]))
		})
	}

	public visitParam(el: ParamsJsonElement){
		var scheme = el.getScheme();
		var object: any = el.getParamObject();
		// validate keys
		Object.keys(object).map((key: any)=> {
			if(!scheme.hasOwnProperty(key)) {
				this.throwMustNotContainError(key)
			}
		})
	}

	public getParamSchemeFromType(type: string){
		switch (type) {
		  case ParamType.number:
				return ParamNumber
			case ParamType.string:
				return ParamString
			case ParamType.date:
					return ParamDate
		  default:
		    throw Error('[INFIJA: Query_engine] unknown query parameter type' + type)
		}
	}

	private throwRequiresError(attr: string) {
		throw Error('[INFIJA: Query_engine] Query requires `'+ attr +'` attribute')
	}

	private throwMustNotContainError(attr: string) {
		throw Error('[INFIJA: Query_engine] Attribute `'+ attr +'` not recognized')
	}

}
