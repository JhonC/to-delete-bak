import KeyValidationVisitor from '../query_engine/visitors/KeyValidationVisitor'
import QueryBuilderVisitor from '../query_engine/visitors/QueryBuilderVisitor'
import QueryJsonElement from '../query_engine/elements/QueryJsonElement'

/**
 * Interface to use QueryEngine architecture
 */

export default abstract class QueryEngine {
  static keyValidation: KeyValidationVisitor = new KeyValidationVisitor
  static queryBuilder: QueryBuilderVisitor = new QueryBuilderVisitor

  public static build(query: any, table: string, scheme: Object): string {
    let el: QueryJsonElement = new QueryJsonElement(table, query, scheme)
    el.accept(QueryEngine.keyValidation)
    el.accept(QueryEngine.queryBuilder)
    return el.getQueryBuilder().build()
  }
  
}
