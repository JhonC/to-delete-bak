import Visitor from '../visitors/Visitor'
import Element from './Element'
import ParamsJsonElement from '../elements/ParamsJsonElement'
import QueryBuilder from '../builders/QueryBuilder'

/**
 * Element abstract class that
 */

export default class ModelJsonElement extends Element {

	public object: any
	public scheme: any
	public params: Array<ParamsJsonElement> = []
	public queryBuilder: QueryBuilder

  /**
   * Constructor.
   */
  constructor(object: any, scheme: Object, queryBuilder: QueryBuilder) {
  	super()
  	this.scheme = scheme
		this.object = object
		this.queryBuilder = queryBuilder
  }

  /**
   * methods.
   */
	public accept(v: Visitor) {
		v.visitModel(this)

		this.params.map((param) => {
			param.accept(v)
		})
	}

	public getModelObject() {
		return this.object;
	}

	public getQueryBuilder() : any{
		return this.queryBuilder;
	}

	public addParam(attrKey: string, type: string, paramObject:any, paramScheme:any) {
		this.params.push(new ParamsJsonElement(attrKey, type, paramObject, paramScheme, this.queryBuilder))
	}

	public getScheme() {
		return this.scheme;
	}
}
