import Visitor from '../visitors/Visitor'
import Element from './Element'
import ModelJsonElement from '../elements/ModelJsonElement'
import QueryBuilder from '../builders/QueryBuilder'
import MysqlQueryBuilder from '../builders/MysqlQueryBuilder'

/**
 * Element abstract class that
 */

export default class QueryJsonElement extends Element {

	public scheme: any
	public target: string
	public queryBuilder : QueryBuilder
	public modelElement: ModelJsonElement = null
	public object: any

  /**
   * Constructor.
   */
  constructor(target: string, object: Object, queryScheme: Object ) {
  	super()
  	this.scheme = queryScheme
  	this.target = target
		this.object = object
		this.queryBuilder = new MysqlQueryBuilder()
  }

  /**
   * methods.
   */
	public accept(v: Visitor) {
		v.visitQuery(this)
		if (this.modelElement == null)
			this.modelElement = new ModelJsonElement(this.object.model, this.scheme, this.queryBuilder);
		this.modelElement.accept(v)
	}

	public setSchemeAttr(key: String, val: string) {
		this.scheme[key.toString()] = val;
	}

	public getQueryBuilder() : any{
		return this.queryBuilder;
	}

	public getScheme() {
		return this.scheme;
	}

	public getModelObject() {
		return this.object;
	}
	public getTarget() {
		return this.target
	}
	public getType() {
		return this.object.type
	}
}
