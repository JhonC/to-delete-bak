import Visitor from '../visitors/Visitor'
import Element from './Element'
import QueryBuilder from '../builders/QueryBuilder'

/**
 * Element abstract class that
 */

export default class ParamsJsonElement extends Element {

	public key: any
	public type: string
	public paramObject: Object
	public paramScheme: Object
	public queryBuilder: QueryBuilder

  /**
   * Constructor.
   */
  constructor(key: string, type: string,  paramObject: Object, scheme: Object, queryBuilder: QueryBuilder) {
  	super()
  	this.key = key
		this.type = type
		this.paramObject = paramObject
		this.paramScheme = scheme
		this.queryBuilder = queryBuilder
  }

  /**
   * methods.
   */
	public accept(v: Visitor) {
		v.visitParam(this)
	}

	public getParamObject() {
		return this.paramObject;
	}

	public getScheme() {
		return this.paramScheme;
	}

	public getType() {
		return this.type;
	}

	public getQueryBuilder() : any {
		return this.queryBuilder;
	}
}
