export const Query = {
    type: 'type',
    model: 'model'
}

export const QueryType = {
    single: 'single',
    list: 'list'
}
