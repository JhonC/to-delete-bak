export const ParamType = {
    string: 'string',
    number: 'number',
    date: 'date'
}

export const ParamString = {
    endsWith: 'endsWith',
    beginsWith: 'beginsWith',
    equals: 'equals'
}

export const ParamNumber = {
    lessThan: 'lessThan',
    greatherThan: 'greatherThan',
    equals: 'equals'
}

export const ParamDate = {
    lessThan: 'lessThan',
    greatherThan: 'greatherThan',
    equals: 'equals'
}
