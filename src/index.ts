import * as http from 'http'
import * as debug from 'debug'
import config from './config'
import DBConnection from './db/DBConnection'

import App from './App'

debug('ts-express:server')

// configure server properties
const port = config.server.port
const host = config.server.host

const server = http.createServer(App)
server.listen(port, host)
server.on('error', onError)
server.on('listening', onListening)

function onError(error: NodeJS.ErrnoException): void {
    if (error.syscall !== 'listen') throw error
    let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port
    switch(error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`)
            process.exit(1)
            break
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`)
            process.exit(1)
            break
        default:
            throw error
    }
}

function onListening(): void {
    let addr = server.address()
    let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`

    console.log(`Listening on `)
    console.log(addr)

    debug(`Listening on ${bind}`)

    // test mysql connection
    let mysql:DBConnection = new DBConnection()
    mysql.testConnection()
}