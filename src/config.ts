const db = {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE
    }
const server = {
        port: 3000,
        host: "0.0.0.0"
    }

const firebase = {
        path: "https://fyp-dev.firebaseio.com"
    }

const env = {
        debug_env: true
    }

export default {server, db, firebase, env}