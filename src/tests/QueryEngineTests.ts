import KeyValidationVisitor from '../query_engine/visitors/KeyValidationVisitor'
import QueryBuilderVisitor from '../query_engine/visitors/QueryBuilderVisitor'
import QueryJsonElement from '../query_engine/elements/QueryJsonElement'
import {ParamType} from '../query_engine/schemes/paramScheme'
import {QueryType} from '../query_engine/schemes/queryScheme'
import BaseTest from './BaseTest'


/**
 * Element abstract class that
 */

export default class QueryEngineTests extends BaseTest {

	public esqueme: Object
	public tableName: String

	public run(){
		this.keyVisitorTest()
	}

	public keyVisitorTest(){
		var el: QueryJsonElement
		var visitor: KeyValidationVisitor = new KeyValidationVisitor()
		var query: Object
		query = {
			type: 'list', // LIST | SINGLE
			model : {
				name: {beginsWith: 'Luc'},
			}
		}
		var scheme: Object = {
			id: ParamType.number,
			name: ParamType.string,
			age: ParamType.number
		}
		el = new QueryJsonElement('table_test', query, scheme)
		el.accept(visitor)

		query = {
			type: 'list', // LIST | SINGLE
			model : {
				unvalid_attr: {beginsWith: 'Luc'},
			}
		}
		el = new QueryJsonElement('table_test', query, scheme)
		try{
			el.accept(visitor)
			this.assertTrue(false)
		} catch(e) {
			this.assertTrue(true)
		}
		// this.assertEqualsString(el.getQuery().toString(), 'SELECT * from table_test WHERE name LIKE "Luc%"' )

		query = {
			type: 'list', // LIST | SINGLE
			model : {
				name: {beginsWithWrong: 'Luc'},
			}
		}
		el = new QueryJsonElement('table_test', query, scheme)
		try{
			el.accept(visitor)
			this.assertTrue(false)
		} catch(e) {
			this.assertTrue(true)
		}
	}

}
