import KeyValidationVisitor from '../query_engine/visitors/KeyValidationVisitor'
import QueryBuilderVisitor from '../query_engine/visitors/QueryBuilderVisitor'
import QueryJsonElement from '../query_engine/elements/QueryJsonElement'
import {ParamType} from '../query_engine/schemes/paramScheme'
import {QueryType} from '../query_engine/schemes/queryScheme'
import MysqlQueryBuilder from '../query_engine/builders/MysqlQueryBuilder'
import BaseTest from './BaseTest'


/**
 * Element abstract class that
 */

export default class QueryEngineTests extends BaseTest {

	public scheme: Object
	public tableName: String

	public run(){
		this.queryProcesserTest()
	}

  /**
   * testing the example object structure
	 * var obj: Object = {
	 *		id:1,
	 *		name: 'name',
	 *		age: 13,
	 *		date: '2020-11-17',
	 *		children: [
	 *		{
	 *				id:1,
	 *				name: 'name',
	 *				age: 13,
	 *				date: '2020-11-17',
	 *			}
	 *		],
	 *		profession: {
	 *			id:1,
	 *			name: 'name',
	 *			age: 13,
	 *			date: '2020-11-17',
	 *	 }
	 * }
   */
	public queryProcesserTest(){
 		var el: QueryJsonElement
 		var visitor: KeyValidationVisitor = new KeyValidationVisitor()
		var builderVisitor: QueryBuilderVisitor = new QueryBuilderVisitor()
 		var query: Object
 		query = {
 			type: 'list', // LIST | SINGLE
 			model : {
 				name: {endsWith: 'p'},
				age: {lessThan: 20},
 				id: {equals: 3},

 			}
 		}
 		var scheme: Object = {
 			id: ParamType.number,
 			name: ParamType.string,
 			age: ParamType.number
 		}
 		el = new QueryJsonElement('table_test', query, scheme)
		el.accept(visitor)
		el.accept(builderVisitor)

		var q: MysqlQueryBuilder = el.getQueryBuilder();

		this.assertTrue(q.target == 'table_test')
		this.assertTrue(q.type == QueryType.list)
		this.assertTrue(q.params.length == 3)
		// console.log(q.build())
		this.assertTrue(q.build() == ` SELECT * FROM table_test WHERE name LIKE '%p' AND age < 20 AND id = 3` )

	}
}
