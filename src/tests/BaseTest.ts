/**
 * Test abstract class
 */

export default abstract class BaseTest {

	public abstract run(): void;

    // public collectMethods(obj: Object) {
    //       let properties = new Set()
    //       let currentObj = obj
    //       do {
    //         Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
    //       } while ((currentObj = Object.getPrototypeOf(currentObj)))
    //       return [...properties.keys()].
	// 	  	filter((item: string) => typeof obj[item] === 'function')
    // }

    public assertTrue(isTrue: boolean): void {
        if(!isTrue)
            this.throwError()
    }

    /**
     * Element abstract class that
     */
    public assertEqualsString(a: string, b: string): void {
        if(a !== b)
            this.throwError()
    }

    public assertEqualsNumber(a: number, b: number): void {
        if(a !== b)
            this.throwError()
    }

    public throwError(): void {
        throw new Error('Assertion failed: Error running test in ' );
    }

}
