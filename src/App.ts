import * as path from 'path'
import * as express from 'express'
import * as logger from 'morgan'
import * as bodyParser from 'body-parser'
import GreetingRoute from './routes/GreetingRoute'
import UserRoute from './routes/UserRoute'
import QueryEngineRoute from './routes/QueryEngineRoute'
import PointRoute from './routes/PointRoute'
/**
 * Creates and configures Express server
 */
class App {

    // ref to Express instance
    public express: express.Application

    /**
     * Constructor.
     */
    constructor() {
        this.express = express()
        this.middleware()
        this.routes()
    }

    /**
     * Configure Express middleware
     */
    private middleware(): void {
        this.express.use(logger('dev'))
        this.express.use(bodyParser.json())
        this.express.use(bodyParser.urlencoded({ extended: true }))
    }

    /**
     * Define Routes
     */
    private routes(): void {
        // initialize express router
        let router: express.Router = express.Router()

        // register routers
        GreetingRoute.register(router)
        UserRoute.register(router)
        PointRoute.register(router)
        QueryEngineRoute.register(router)
        //use router middleware
        this.express.use(router)
    }

}

export default new App().express
