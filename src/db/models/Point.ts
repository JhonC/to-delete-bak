export interface PointData {
    id?: number
    lat: number
    lon: number
    createdAt?: string
    updatedAt?: string
}

export abstract class PointProvider {

    public static table:string = 'location'

    public static deletePointById(id: number) : string {
        return `DELETE FROM P${PointProvider.table} WHERE id = ${id}`
    }

    public static insertPoint(point: PointData) : string {
        return `INSERT INTO ${PointProvider.table} (lat, lon) VALUES (${point.lat}, ${point.lon})`
    }

    public static findById(id: number) : string {
        return `SELECT * FROM ${PointProvider.table} WHERE id = ${id}`
    }
    public static findAll() : string {
        return `SELECT * FROM ${PointProvider.table}`
    }
    public static findByLatLon(lat: number, lon: number) : string {
        return `SELECT * FROM ${PointProvider.table} WHERE lat = ${lat} AND lon = ${lat}`
    }
    public static findByRadius(latCenter: number, lonCenter: number, radius: number) : string {
           // TODO: test this query
        // return `SELECT *,(((acos(sin((${lat} * pi()/180)) *
        //             sin(( lat * pi()/180)) + cos((${lat} * pi()/180)) *
        //             cos(( lat * pi()/180)) * cos(((${lon} - lon)*
        //             pi()/180)))) * 180/pi()) * 60 * 1.1515
        //         ) as distance
        //         FROM ${UserProvider.table}
        //         HAVING distance <= ${radius}`

        return `SELECT * , (6371e3 * (2 *
                        atan(
                            sqrt(sin(radians(${latCenter}-lat)/2) *
                                sin(radians(${latCenter}-lat)/2) +
                                cos(radians(lat)) * cos(radians(${latCenter})) *
                                sin(radians(${lonCenter}-lon)/2) * sin(radians(${lonCenter}-lon)/2)),
                            sqrt(1-(sin(radians(${latCenter}-lat)/2) *
                                    sin(radians(${latCenter}-lat)/2) +
                                    cos(radians(lat)) * cos(radians(${latCenter})) *
                                    sin(radians(${lonCenter}-lon)/2) * sin(radians(${lonCenter}-lon)/2)
                                )
                            )
                        )
                    )
        ) as distance
                FROM ${PointProvider.table}
                HAVING distance <= radians(${radius})`


    }

}
