export interface UserData {
    id?: number
    nickname?: string
    password?: string
    lat?: number
    lon?: number
    notificationToken?: string
    isAnonymous?: boolean
    isLoggedIn?: boolean
    createdAt?: string
    updatedAt?: string
    lastViewed?: string
}

export interface LoginData {
    nickname?: string
    password?: string
}


export abstract class UserProvider {

    public static table: string = 'user'

    public static insertAnonimous(user: UserData) : string {
        return `INSERT INTO ${UserProvider.table} (lat, lon, isAnonymous) VALUES (${user.lat}, ${user.lon},1)`
    }

    public static updateLocation(user: UserData) : string {
        return `UPDATE ${UserProvider.table} SET lat = ${user.lat}, lon = ${user.lon}, notificationToken = "${user.notificationToken}" WHERE id = ${user.id}`
    }

    public static updatePassword(user: UserData) : string {
        return `UPDATE ${UserProvider.table} SET password = '${user.password}' WHERE id = ${user.id}`
    }

    public static registerBrandNewUser(user: UserData) : string {
        return `INSERT INTO ${UserProvider.table} (nickname, password, lat, lon, isAnonymous) VALUES ('${user.nickname}', '${user.password}', ${user.lat}, ${user.lon}, 0)`
    }

    public static updateLoggedIn(user: UserData, isLoggedIn: number) : string {
        return `UPDATE ${UserProvider.table} SET isLoggedIn = ${isLoggedIn} WHERE id = ${user.id}`
    }
    
    public static updateRegisteredUser(user: UserData) : string {
        return `UPDATE ${UserProvider.table} SET nickname = "${user.nickname}", isAnonymous = 0 WHERE id = ${user.id}`
    }
    
    public static updateLastViewed(id: number) : string {
        return `UPDATE ${UserProvider.table} SET lastViewed = NOW() WHERE id = ${id}`
    } 

    public static findById(id: number) : string {
        return `SELECT * FROM ${UserProvider.table} WHERE id = ${id}`
    }

    public static findByIdAndNickname(user: UserData) {
        return `SELECT * FROM ${UserProvider.table} WHERE id = ${user.id} AND nickname = '${user.nickname}'`
    }

    public static findByNickname(nickname: string) : string {
        return `SELECT * FROM ${UserProvider.table} WHERE nickname = '${nickname}'`
    }

    public static findByNicknameAndPassword(user: UserData) : string {
        return `SELECT * FROM ${UserProvider.table} WHERE password = '${user.password}' AND nickname = '${user.nickname}'`
    }

    public static deleteUserById(id: number) : string {
        return `DELETE FROM ${UserProvider.table} WHERE id = ${id}`
    }

    public static findAll() : string {
        return `SELECT * FROM ${UserProvider.table}`
    }
}