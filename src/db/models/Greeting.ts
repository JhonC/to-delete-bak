export interface GreetingData {
    id?: number
    description: string
    createdAt?: string
    updatedAt?: string
    lastViewed?: string
}

export abstract class GreetingProvider {

    public static table:string = 'test_table'
    
    public static deleteById(id: number) : string {
        return `DELETE FROM P${GreetingProvider.table} WHERE id = ${id}`
    }

    public static insert(greeting: GreetingData) : string {
        return `INSERT INTO ${GreetingProvider.table} (description) VALUES ('${greeting.description}')`
    }

    public static findById(id: number) : string {
        return `SELECT * FROM ${GreetingProvider.table} WHERE id = ${id}`
    }
    public static findAll() : string {
        return `SELECT * FROM ${GreetingProvider.table}`
    }
    
}