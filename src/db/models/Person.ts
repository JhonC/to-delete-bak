import {ParamType} from '../../query_engine/schemes/paramScheme'

export interface PersonData {
    id?: number
    name?: string
    age?: number
    bornDate?: number
    createdAt?: string
    updatedAt?: string

}

export const  PersonScheme: Object = {
  id: ParamType.number,
  name: ParamType.string,
  age: ParamType.number,
  bornDate: ParamType.date
}

export abstract class PersonProvider {

    public static table:string = 'person'

    public static deleteById(id: number) : string {
        return `DELETE FROM P${PersonProvider.table} WHERE id = ${id}`
    }

    public static insert(person: PersonData) : string {
        return `INSERT INTO ${PersonProvider.table} (name, age, bornDate) VALUES ('${person.name}', ${person.age}, STR_TO_DATE('${person.bornDate}','%Y-%m-%d'))`
    }

    public static findById(id: number) : string {
        return `SELECT * FROM ${PersonProvider.table} WHERE id = ${id}`
    }
    public static findAll() : string {
        return `SELECT * FROM ${PersonProvider.table}`
    }
}
