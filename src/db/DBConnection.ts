import * as mysql from 'mysql'
import config from '../config'
import { IConnectionConfig, IPool, IConnection, IPoolConfig } from 'mysql'
import { ResponseError } from "../routes/ResponseError"

/**
 * Configure Mysql connection
 */
export default class DBConnection {

    private pool: IPool

    constructor() {
        this.pool = this.createPoolConnection()
    }

    public createPoolConnection(): IPool {

        let connConf: IPoolConfig = {
                    host: config.db.host,
                    port: parseInt(config.db.port),
                    user: config.db.user,
                    password: config.db.password,
                    database: config.db.database,
                    connectionLimit: 10
                }

        return mysql.createPool(connConf)
    }

    public createConnection(): mysql.IConnection {
        let connConf: IConnectionConfig = {
                    host: config.db.host,
                    port: parseInt(config.db.port),
                    user: config.db.user,
                    password: config.db.password,
                    database: config.db.database,
                    multipleStatements:true
                }

        return mysql.createConnection(connConf)
    }

    public executeQuery(sqlInstruction: string, errorCallback: Function, successCallback: Function) : void {
        console.log(sqlInstruction);
        var _this = this
        this.pool.getConnection(function(err, connection) {
            if (err) {
                this.handleDBError(errorCallback, sqlInstruction, err)
                return
            }
            // Use the connection
            connection.query(sqlInstruction, function (err2, results, fields) {
                // done with the connection.
                connection.release();
                // Handle error after the release.
                if (err2) {
                    _this.handleDBError(errorCallback, sqlInstruction, err2)
                    return
                }
                successCallback(results)
            });
        });
    }

    /**
     *
     * @param sqlInstruction
     * @param errorCallback
     * @param callback
     */
    public executeSqlFile(sqlInstruction: string, errorCallback: Function, callback: Function) : void {
        let connection: IConnection = this.createConnection()
        connection.connect((err: object) => {
            if (err) {
                this.handleDBError(errorCallback, sqlInstruction, err)
                return
            }

            connection.query(sqlInstruction, (err: object, result: object) => {
                if (err) {
                    this.handleDBError(errorCallback, sqlInstruction, err)
                    return
                }

                console.log(sqlInstruction, result)

                callback(result)
            })
        })
    }

    private handleDBError(errorCallback: Function, sqlInstruction: string, err: any) {
        let message: any
        let code: any

        // parse error messages from mysql
        switch (err.errno) {
            case 1292:
                code = 400
                message = "Invalid request"
                break
            case 1062:
                code = 409
                message = "Duplicated"
                break
            case 1054:
                code = 400
                message = "Invalid request"
                break
            case 1452:
                code = 400
                message = "Invalid request"
                break
            default:
                message = "INFIJA: Unexpected error, try again later"
                break
        }

        // show error details if debug env
        if (config.env.debug_env)
            message = err

        // build response
        let response: ResponseError = {
            code: code,
            error: { message: message}
        }

        errorCallback(response)
        console.error(sqlInstruction, err)
        // throw err // error already handled
    }

    public testConnection() : void {
        let connConf: IConnectionConfig = {
                    host: config.db.host,
                    port: parseInt(config.db.port),
                    user: config.db.user,
                    password: config.db.password,
                    database: config.db.database,
                    multipleStatements:true
                }

        let connection = mysql.createConnection(connConf)
        connection.connect(function(err: object) {
            if (err)
                console.log(err)
            else
                console.log('You are now connected to MySql')
            })
    }

}
