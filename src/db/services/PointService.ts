import DBConnection from '../DBConnection'
import { PointData, PointProvider } from '../models/Point'
import { ResponseError } from "../../routes/ResponseError"
// import { CloudMessageManager, NotificationPayload } from "../firebase/CloudMessageManager"

export default class PointService {

    private db: DBConnection

    constructor(){
        this.db = new DBConnection
    }
    public findPointsByLatLon(lat: number, lon: number, error: Function, success: Function) {
        let sql: string = PointProvider.findByLatLon(lat, lon)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findPointsByRadius(lat: number, lon: number, rad: number, error: Function, success: Function) {
        let sql: string = PointProvider.findByRadius(lat, lon, rad)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findPoints(error: Function, success: Function) {
        let sql: string = PointProvider.findAll()
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }
    public findPointsById(id: number, error: Function, success: Function) {
        let sql: string = PointProvider.findById(id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public deletePoint(id: number, error: Function, success: Function) {
        let sql: string = PointProvider.deletePointById(id)
        this.db.executeQuery(sql, error, (result: any)=> {
            if (result.affectedRows > 0)
                success({mess: result}) // default response for deletion
            else
                error(<ResponseError>{code:404, error:{message:"Not found"}})
        })
    }

    public createPoint(point: PointData, error: Function, success: Function) {
        let sql: string = PointProvider.insertPoint(point)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }



}
