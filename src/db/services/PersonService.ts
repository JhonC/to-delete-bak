import DBConnection from '../DBConnection'
import { PersonData, PersonProvider, PersonScheme } from '../models/Person'
import { ResponseError } from "../../routes/ResponseError"
import QueryEngine from '../../query_engine/QueryEngine'
// import { CloudMessageManager, NotificationPayload } from "../firebase/CloudMessageManager"

export default class PersonService {

    private db: DBConnection

    constructor(){
        this.db = new DBConnection
    }

    public create(greeting: PersonData, error: Function, success: Function) {
        let sql: string = PersonProvider.insert(greeting)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }


    public query(query: any, error: Function, success: Function) {
        let sql: string = QueryEngine.build(query, PersonProvider.table, PersonScheme)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findAll(error: Function, success: Function) {
        let sql: string = PersonProvider.findAll()
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

}
