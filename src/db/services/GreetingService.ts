import DBConnection from '../DBConnection'
import { GreetingData, GreetingProvider } from '../models/Greeting'
import { ResponseError } from "../../routes/ResponseError"
// import { CloudMessageManager, NotificationPayload } from "../firebase/CloudMessageManager"

export default class GreetingService {

    private db: DBConnection

    constructor(){
        this.db = new DBConnection
    }

    public deleteById(id: number, error: Function, success: Function) {
        let sql: string = GreetingProvider.deleteById(id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findById(id: number, error: Function, success: Function) {
        let sql: string = GreetingProvider.findById(id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findAll(error: Function, success: Function) {
        let sql: string = GreetingProvider.findAll()
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public create(greeting: GreetingData, error: Function, success: Function) {
        let sql: string = GreetingProvider.insert(greeting)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }
}
