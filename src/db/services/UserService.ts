import DBConnection from '../DBConnection'
import {UserData, UserProvider} from '../models/User'
import { ResponseError } from "../../routes/ResponseError"


export default class UserService {

    private db: DBConnection

    constructor(){
        this.db = new DBConnection
    }

    public getAll(error: Function, success: Function) {
        let sql: string = UserProvider.findAll()
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public login(user: UserData, error: Function, success: Function) {
        let sql: string = UserProvider.findByNicknameAndPassword(user)
        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length == 1) { // user exists
                // remove temporal user
                //TODO: avoid remove user because it removes pets
                user = result[0]
                sql = UserProvider.updateLoggedIn(user, 1) // loggedin 
                this.db.executeQuery(sql, error, (result: any)=> {
                    user.isLoggedIn = true;
                    success(user)
                })
            } else if (result.length > 1) {
                let response: ResponseError = {
                    code:404,
                    error:{message: "Duplicated users, database compromised"}
                }
                error(response)
            } else {
                let response: ResponseError = {
                    code:404,
                    error:{message: "User not found"}
                }
                error(response)
            }
        })
    }

    public logout(user: UserData, error: Function, success: Function) {
        let sql: string = UserProvider.findById(user.id)
        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length > 0) { // nickname found in a diferent user
                user = result[0]
                sql = UserProvider.updateLoggedIn(user, 0) // loggedin 
                this.db.executeQuery(sql, error, (result: any)=> {
                    console.log(result);
                    user.isLoggedIn = false;
                    success(user)
                })
            } else {
                let response: ResponseError = {
                    code:404,
                    error:{message: "User not found"}
                }
                error(response)
            }
        })
    }

    public updatePassword(user: UserData, error: Function, success: Function) {
        let sql: string = UserProvider.findById(user.id)
        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length > 0 && user.id == result[0].id) { // user exists
                console.log(result)
                sql = UserProvider.updatePassword(user)
                this.db.executeQuery(sql, error, (result: any)=> {
                    console.log(result)
                    success(result[0])
                })
            } else {
                let response: ResponseError = {
                    code:404,
                    error:{message: "User not found"}
                }
                error(response)
            }
        })
    }

    public getUserById(id:number, error: Function, success: Function) {
        let sql: string = UserProvider.findById(id)
        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length > 0) // user exists
                success(result[0])
            else {
                error(<ResponseError>{
                    code:404,
                    error:{message: "User not found"}
                })
            }
        })
    }

    public getUserByNickname(nickname:string, error: Function, success: Function) {
        let sql: string = UserProvider.findByNickname(nickname)
        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length > 0) // user exists
                success(result[0])
            else {
                error(<ResponseError>{
                    code:404,
                    error:{message: "User not found"}
                })
            }
        })
    }

    public registerBrandNewUser(data: UserData, error: Function, success: Function) {
        let sql: string = UserProvider.findByNickname(data.nickname)

        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length <= 0) {
                console.log(data)
                sql = UserProvider.registerBrandNewUser(data)
                this.db.executeQuery(sql, error, (result: any)=> {
                    console.log(result)
                    success({id: result.insertId})
                })
            } else {
                error(<ResponseError> {
                    code:409,
                    error:{message: "User found, please try another"}
                })
            }
        })
    }

    public registerDeviceLocation(data: UserData, error: Function, success: Function) {
        let sql: string

        if (data.id == null) {
            sql = UserProvider.insertAnonimous(data)
            this.db.executeQuery(sql, error, (result: any)=> {
                success({id: result.insertId})
            })
        } else {
            sql = UserProvider.findById(data.id)
            this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
                if (result.length > 0) {
                    sql = UserProvider.updateLocation(data)
                    this.db.executeQuery(sql, error, (result: any)=> {
                        success(data)
                    })
                } else {
                    sql = UserProvider.insertAnonimous(data)
                    this.db.executeQuery(sql, error, (result: any)=> {
                        success({id: result.insertId})
                    })
                }
            })
        }
    }

    // @Deprecated
    public registerNewUser(data: UserData, error: Function, success: Function) {
        let sql: string = UserProvider.findByNickname(data.nickname)

        this.db.executeQuery(sql, error, (result: Array<UserData>)=> {
            if (result.length <= 0) {
                console.log(data)
                sql = UserProvider.updateRegisteredUser(data)
                this.db.executeQuery(sql, error, (result: any)=> {
                    success(data)
                })
            } else {
                error(<ResponseError> {
                    code:409,
                    error:{message: "User found, please try another"}
                })
            }
        })
    }

    public registerAnonymousUser(data: UserData, error: Function, success: Function) {
        console.log(data)
        let sql: string = UserProvider.insertAnonimous(data)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }
    
    public deleteUser(data: UserData, error: Function, success: Function) {
        console.log(data)
        let sql: string = UserProvider.deleteUserById(data.id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id:data.id, message: "User deleted"})
        })
    }
    
    public updateLastViewed(data: UserData, error: Function, success: Function) {
        console.log(data)
        let sql: string = UserProvider.updateLastViewed(data.id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id:data.id, message: "User last Viewed Updated"})
        })
    }
}