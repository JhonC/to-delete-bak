const gulp = require('gulp')
const nodemon = require('gulp-nodemon')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('tsconfig.json')

/**
 * Compile Typescript files under /src folder
 * and save them under dist folder
 */
gulp.task('compile', () => {
    console.log("Compiling ...........")
    const stream = tsProject.src()
        .pipe(tsProject())
        .js
        .pipe(gulp.dest('dist'))

    return stream
})

/**
 * Watch changes under src folder using nodemon
 */
// using gulp.series creates a loop
// gulp.task('default', gulp.series('compile'), function (done) {
gulp.task('default', function (done) {
    var stream = nodemon({
                 script: 'dist/index.js' // run ES5 code
               , watch: 'src' // watch ES2015 code
               , tasks: ['compile'] // compile synchronously onChange
               , done: done
               })

    return stream
})

/**
 * Copy config files
 */
// gulp.task('copy', function () {
//     gulp.src('./src/*.json')
//         .pipe(gulp.dest('./dist'));
// })

// gulp.task('default', gulp.parallel('copy', 'watch'))
